import { Component, OnDestroy, OnInit } from '@angular/core';
import { EnvAuthService } from '../../services/env/env.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Env } from '../../interfaces/env.interface';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

interface Response {
  codigo: number;
  descripcion: string;
  success: boolean;
}

interface LoginResponse extends Response {
  respuesta: LoginResponseRespuesta;
}

interface MenuOptionResponse extends Response {
  respuesta: MenuOption[];
}

interface LoginResponseRespuesta {
  response: CoreResponse;
  roleEstatus: number;
  tokenInformation: TokenInformation;
  userInformation: UserInformation;
}

interface TokenInformation {
  token: string;
  expirationDate: string;
}

interface UserInformation {
  email?: string;
  employeeNumber: string;
  userName: string;
  paternalName: string;
  maternalName: string;
  menuOptions?: MenuOptionResponse;
  roles?: any;
}

interface Response {
  code: string;
  message: string;
  serverDate: string;
  type: boolean;
}

interface MenuOption {
  levelCoord: string;
  menuId: string;
  menuName: string;
  parentId: string;
  subMenus: MenuOption[];
  url: string;
}

interface CoreResponse {
  message: string;
  code: string;
  type: boolean;
  serverDate: string;
}

interface Role {
  generateGroup: string;
  key: number;
  value: string;
}

@Component({
  selector: 'auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  private settings: Env;

  private params: Params;

  menus: Subject<any>;

  constructor(
    private env: EnvAuthService,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) {
    this.menus = new Subject<any>();
    this.settings = this.env.getProperties();
    this.params = this.activatedRoute.snapshot.queryParams;
    if (Object.keys(this.params).length === 0) {
      this.builldRedirecToMasterKeyRequest();
      return;
    }
  }
  ngOnDestroy(): void {
    this.menus.unsubscribe();
  }

  ngOnInit(): void {
    sessionStorage.setItem('MK_CODE', this.params.code);
    sessionStorage.setItem('MK_SCOPE', this.params.scope);
    this.menus.subscribe(data => console.log('RESOLVIENDO::', data));
    this.login();
  }

  builldRedirecToMasterKeyRequest(): void {
    const uri =
      `${this.settings.MASTER_KEY_URL}` +
      `response_type=code&` +
      `client_id=${this.settings.CLIENT_ID}&` +
      `client_secret=${this.settings.CLIENT_SECRET}&` +
      `redirect_uri=${this.settings.URL_REDIRECT}&` +
      `scope=${this.settings.SCOPE}&` +
      `acr_values=${this.settings.ACR_VALUES}`;
    window.location.href = `${uri}`;
  }

  login(): void {
    const body = {
      grant_type: 'authorization_code',
      client_id: `${this.settings.CLIENT_ID}`,
      client_secret: `${this.settings.CLIENT_SECRET}`,
      redirect_uri: `${this.settings.URL_REDIRECT}`,
      code: `${sessionStorage.getItem('MK_CODE')}`
    };

    if (!sessionStorage.getItem('TOKEN_CORE')) {
      this.http.post<LoginResponse>(`${this.settings.CORE}login`, body).pipe(
        tap(data => {
          sessionStorage.setItem('TOKEN_CORE', `${data.respuesta.tokenInformation.token}`);
          sessionStorage.setItem('TOKEN_EXPIRATION_CORE', `${data.respuesta.tokenInformation.expirationDate}`);
          sessionStorage.setItem('ROLE_CORE', `${data.respuesta.roleEstatus}`);
          sessionStorage.setItem('UI_CORE', `${JSON.stringify(data.respuesta.userInformation)}`);
          return data;
        })
      ).subscribe(data => this.menus.next(data.respuesta.userInformation));
    } else {
      this.menus.next(JSON.parse(sessionStorage.getItem('UI_CORE')));
    }
  }

  logout(): void {
    window.location.href = `${this.settings.LOGOUT}`;
    sessionStorage.clear();
  }

}
