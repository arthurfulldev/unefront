import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [{
  path: '',
  component: AuthComponent,
  children: [
    {
      path: '',
      component: LoginComponent
    }
  ]
}];

@NgModule({
  declarations: [AuthComponent, LoginComponent],
  imports: [
    RouterModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule, AuthComponent]
})
export class AuthRoutingModule { }
