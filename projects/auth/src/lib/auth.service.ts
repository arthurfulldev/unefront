import { Injectable } from '@angular/core';
import { Env } from './interfaces/env.interface';
import { EnvAuthService } from './services/env/env.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private settings: Env;

  constructor(private env: EnvAuthService) {
    this.settings = env.getProperties();
  }

  logout(): void {
    window.location.href = `${this.settings.LOGOUT}`;
    sessionStorage.clear();
  }
}
